<?php

/**
 * Implementation of hook_taxonomy_default_vocabularies().
 */
function answers_taxonomy_taxonomy_default_vocabularies() {
  return array(
    'answers' => array(
      'name' => 'Answers',
      'description' => '',
      'help' => '',
      'relations' => '1',
      'hierarchy' => '0',
      'multiple' => '1',
      'required' => '0',
      'tags' => '1',
      'module' => 'features_answers',
      'weight' => '0',
      'nodes' => array(
        'question' => 'question',
      ),
    ),
  );
}
